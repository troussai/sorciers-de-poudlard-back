'use strict';

const mongoose = require('mongoose');
const boom = require('@hapi/boom');
const userSchema = require('../../schemas/UserSchema');
const User = new mongoose.model('User', userSchema);

const signUP = function (req, res, next) {
    // Test if confirmPassword is egal to password
    if (req.body.user.password !== req.body.user.confirmPassword) {
        return next(boom.badRequest("Password and confirmPassword aren't the same"));
    }
    // Test if the email address is already used
    User.findOne({email: req.body.user.email}, function (err, user) {
        if (user !== null) {
            return next(boom.badRequest("The mail address is already used"));
        } else {
            // Create the user
            User.create({
                email: req.body.user.email,
                password: req.body.user.password
            }, function (err, user) {
               if (err) return next(boom.badRequest(err));
               res.json({
                   status: 'success',
                   message: `User ${user.email} have been created`
               });
            });
        }
    });
};

module.exports = signUP;
