'use strict';

const mongoose = require('mongoose');
const config = require('../../config/default');
const boom = require('@hapi/boom');
const userSchema = require('../../schemas/UserSchema');
const User = new mongoose.model('User', userSchema);
const characterSchema = require('../../schemas/CharacterSchema');
const Character = new mongoose.model('Character', characterSchema);
const jwt = require('jsonwebtoken');

const authenticator = {
    isAuthenticated: function (req, res, next) {
        // Get the token
        let token = req.headers.authorization.replace('Bearer ', '');
        // Test the token
        jwt.verify(token, config.secretKey, function (err, decoded) {
            if (err) {
                return next(boom.forbidden("Invalid token"));
            } else {
                // Set the user in the request
                User.findById(decoded.userId, "-password", function (err, user) {
                    if (err) return next(boom.badRequest("User not found"));
                    req.currentUser = user;
                    next();
                });
            }
        });
    },

    isPlayer: function (req, res, next) {
        // Get the token
        let token = req.headers.authorization.replace('Bearer ', '');
        // Test the token
        jwt.verify(token, config.secretKey, function (err, decoded) {
            if (err) {
                return next(boom.forbidden("Invalid token"));
            } else {
                Character.findById(decoded.characterId, function (err, character) {
                   if (err) return next(boom.badRequest("Character not found"));
                   // Test if the character is link to this user
                   if (character.userId != decoded.userId) return next(boom.forbidden("It isn't y'oure character"));
                   req.currentCharacter = character;
                   next();
                });
            }
        })
    }
};

module.exports = authenticator;
