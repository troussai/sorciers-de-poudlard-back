'use strict';

const express = require('express');
const router = express.Router();

// Import the different controller
const securityController = require('../controllers/security');

/**
 * Routes for account creation and login
 */
router.post('/sign-up', securityController.sign_up);


module.exports = router;
