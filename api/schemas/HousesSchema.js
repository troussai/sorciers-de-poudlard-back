const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const houseSchema = new Schema({
   name: {
       type: String,
       required: true
   },
    points: {
       type: Number,
        default: 0
    },
    icon: {
       type: String,
    }
});
