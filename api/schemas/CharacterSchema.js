const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const characterSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    sexe: {
        type: String,
        required: true,
        enum: ['m', 'f']
    },
    age: {
        type: Number,
        required: true
    },
    house: {
        type: Schema.Types.ObjectId,
        ref: "houses"
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: "users"
    }
});

module.exports = characterSchema;
